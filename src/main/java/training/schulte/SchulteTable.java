package training.schulte;

import static training.schulte.util.GameUtil.getClassPathResourceUrl;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import training.schulte.manager.SingleInstanceManager;

/**
 * Starts application.
 * @author Alexey Chalov
 */
public final class SchulteTable extends Application {

    private Stage stage;
    private static SingleInstanceManager singleInstanceManager;
    private static final DataBridge DATA_BRIDGE = DataBridge.instance();

    private static final String NO_INSTANCE_CHECK_OPTION = "noinstcheck";

    /**
     * Starts application.
     * @param args program arguments
     * @throws Exception if error rise
     */
    public static void main(String[] args) throws Exception {
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        options.addOption(new Option(NO_INSTANCE_CHECK_OPTION, false, null));
        CommandLine cLine = parser.parse(options, args, true);

        boolean skipSingleInstanceCheck = cLine.hasOption(NO_INSTANCE_CHECK_OPTION);
        if (skipSingleInstanceCheck || (singleInstanceManager = SingleInstanceManager.instance()).performActions()) {
            launch();
        }
    }

    @Override
    public void stop() {
        if (singleInstanceManager != null) {
            singleInstanceManager.closeChannel();
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClassPathResourceUrl("image/logo.png").toExternalForm()));

        DATA_BRIDGE.setApplication(this);

        buildStage();
    }

    /**
     * Builds stage.
     */
    @SneakyThrows
    public void buildStage() {
        Scene scene = new Scene(new FXMLLoader(getClassPathResourceUrl("view/Main.fxml"), DATA_BRIDGE.getResourceBundle()).load());
        scene.getStylesheets().add(getClassPathResourceUrl("style/application.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle(DATA_BRIDGE.getResourceBundle().getString("title"));
        stage.show();
    }

    /**
     * Returns current {@link Stage} instance.
     * @return {@link Stage} instance
     */
    public Stage getStage() {
        return stage;
    }
}
