package training.schulte.listcell;

import static training.schulte.util.GameUtil.getClassPathResourceUrl;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import training.schulte.DataBridge;
import training.schulte.util.GameUtil;

/**
 * {@link ListCell} implementation for locales.
 * @author Alexey Chalov
 */
public final class LocaleListCell extends ListCell<Locale> {

    private ResourceBundle bundle = DataBridge.instance().getResourceBundle();

    @Override
    protected void updateItem(Locale locale, boolean empty) {
        super.updateItem(locale, empty);
        if (locale != null) {
            String label = null;
            String iconResource = null;
            if (GameUtil.LOCALE_RU.equals(locale)) {
                label = bundle.getString("main.language.ru");
                iconResource = "image/locale/ru_RU.png";
            }
            if (GameUtil.LOCALE_UK.equals(locale)) {
                label = bundle.getString("main.language.en");
                iconResource = "image/locale/en_GB.png";
            }
            ImageView iconImageView = new ImageView(new Image(getClassPathResourceUrl(iconResource).toExternalForm()));
            setText(label);
            setGraphic(iconImageView);
        }
    }
}
