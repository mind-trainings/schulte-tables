package training.schulte.listcell;

import java.util.ResourceBundle;

import javafx.scene.control.ListCell;
import training.schulte.DataBridge;
import training.schulte.model.Interval;

/**
 * {@link ListCell} implementation for time intervals.
 * @author Alexey Chalov
 */
public final class IntervalListCell extends ListCell<Interval> {

    private ResourceBundle bundle = DataBridge.instance().getResourceBundle();

    @Override
    protected void updateItem(Interval item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setText(bundle.getString(item.name()));
        }
    }
}
