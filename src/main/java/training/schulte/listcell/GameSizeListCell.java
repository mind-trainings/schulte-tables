package training.schulte.listcell;

import javafx.scene.control.ListCell;
import training.schulte.model.GameSize;

/**
 * {@link ListCell} implementation for game sizes.
 * @author Alexey Chalov
 */
public class GameSizeListCell extends ListCell<GameSize> {

    @Override
    protected void updateItem(GameSize item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setText(String.format("%dx%d", item.getWidth(), item.getHeight()));
        }
    }
}
