package training.schulte.listcell;

import java.util.ResourceBundle;

import javafx.scene.control.ListCell;
import training.schulte.DataBridge;
import training.schulte.model.GameType;

/**
 * {@link ListCell} implementation for game types.
 * @author Alexey Chalov
 */
public final class GameTypeListCell extends ListCell<GameType> {

    private ResourceBundle bundle = DataBridge.instance().getResourceBundle();

    @Override
    protected void updateItem(GameType item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setText(bundle.getString(item.name()));
        }
    }
}
