package training.schulte.celldata;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import training.schulte.model.CellData;
import training.schulte.model.GameSize;
import training.schulte.model.GameType;

/**
 * Cell data generator.
 * @author Alexey Chalov
 */
public class CellDataGenerator {

    private CellDataCreator creator;

    /**
     * Constructor.
     * @param gameType {@link GameType} constant
     * @param settings game settings
     */
    public CellDataGenerator(GameType gameType, Map<String, String> settings) {
        switch (gameType) {
            case CLASSICAL_NUMERIC:
                creator = new ClassicalNumericCellDataCreator(settings);
                break;
            case BLACK_RED_NUMERIC:
                creator = new BlackRedNumericCellDataCreator(settings);
            default:
                break;
        }
    }

    /**
     * Generates new cell data.
     * @param gameSize {@link GameSize} instance
     * @return list of cell data
     */
    public List<CellData> generate(GameSize gameSize) {
        List<CellData> cells = creator.newCellData(gameSize);
        Collections.shuffle(cells);
        return cells;
    }

    /**
     * Delegate method to {@link CellDataCreator#nextCell()}.
     * @return {@link CellData} instance
     */
    public CellData nextCell() {
        return creator.nextCell();
    }

    /**
     * Delegate method to {@link CellDataCreator#currentCell()}.
     * @return {@link CellData} instance
     */
    public CellData currentCell() {
        return creator.currentCell();
    }

    /**
     * Delegate method to {@link CellDataCreator#hasMoreElements()}.
     * @return boolean
     */
    public boolean hasMoreElements() {
        return creator.hasMoreElements();
    }
}
