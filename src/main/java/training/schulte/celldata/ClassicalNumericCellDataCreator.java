package training.schulte.celldata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.scene.paint.Color;
import training.schulte.model.CellData;
import training.schulte.model.GameSize;
import training.schulte.model.setting.ClassicalNumericSetting;

/**
 * Cell data creator for classical game type.
 * @author Alexey Chalov
 */
class ClassicalNumericCellDataCreator implements CellDataCreator {

    private List<CellData> cells;
    private int currentIndex;

    private Map<ClassicalNumericSetting, String> settings;

    /**
     * Constructor.
     * @param settings game settings map
     */
    ClassicalNumericCellDataCreator(Map<String, String> settings) {
        this.settings = settings.entrySet().stream()
                                .collect(Collectors.toMap(k -> ClassicalNumericSetting.valueOf(k.getKey()), Map.Entry::getValue));
    }

    @Override
    public List<CellData> newCellData(GameSize gameSize) {
        cells = IntStream.range(1, gameSize.getWidth() * gameSize.getHeight() + 1)
                         .mapToObj(i -> new CellData(String.valueOf(i), String.valueOf(i), null, null, Color.GRAY))
                         .collect(Collectors.toList());
        if (Integer.parseInt(settings.get(ClassicalNumericSetting.COUNTDOWN)) == 1) {
            Collections.reverse(cells);
        }
        currentIndex = -1;
        return new ArrayList<>(cells);
    }

    @Override
    public CellData nextCell() {
        return cells.get(++currentIndex);
    }

    @Override
    public CellData currentCell() {
        return cells.get(currentIndex);
    }

    @Override
    public boolean hasMoreElements() {
        return currentIndex < cells.size() - 1;
    }
}
