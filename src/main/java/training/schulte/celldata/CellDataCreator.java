package training.schulte.celldata;

import java.util.List;

import training.schulte.model.CellData;
import training.schulte.model.GameSize;

/**
 * Common interface for cell data creators.
 * @author Alexey Chalov
 */
interface CellDataCreator {

    /**
     * Creates new cell data.
     * @param gameSize {@link GameSize} instance
     * @return list of {@link CellData} instances
     */
    List<CellData> newCellData(GameSize gameSize);

    /**
     * Returns next cell to search.
     * @return next cell to search
     */
    CellData nextCell();

    /**
     * Returns current cell.
     * @return current cell
     */
    CellData currentCell();

    /**
     * Returns true, if all cells were found, false otherwise.
     * @return boolean
     */
    boolean hasMoreElements();
}
