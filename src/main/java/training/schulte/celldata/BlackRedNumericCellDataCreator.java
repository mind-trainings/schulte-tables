package training.schulte.celldata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.scene.paint.Color;
import training.schulte.model.CellData;
import training.schulte.model.GameSize;
import training.schulte.model.setting.BlackRedNumericSetting;

/**
 * Cell data creator for black-red numeric game type.
 * @author Alexey Chalov
 */
class BlackRedNumericCellDataCreator implements CellDataCreator {

    private List<CellData> cells;
    private int currentIndex;

    private Map<BlackRedNumericSetting, String> settings;

    /**
     * Constructor.
     * @param settings game settings map
     */
    BlackRedNumericCellDataCreator(Map<String, String> settings) {
        this.settings = settings.entrySet().stream()
                                .collect(Collectors.toMap(k -> BlackRedNumericSetting.valueOf(k.getKey()), Map.Entry::getValue));
    }

    @Override
    public List<CellData> newCellData(GameSize gameSize) {
        int totalCellsNumber = gameSize.getWidth() * gameSize.getHeight();
        int redCellsNumber = totalCellsNumber / 2;
        int blackCellsNumber = totalCellsNumber % 2 == 1 ? redCellsNumber + 1 : redCellsNumber;

        boolean colorizeBg = Integer.parseInt(settings.get(BlackRedNumericSetting.COLORIZE_BACKGROUND)) == 1;
        String redCellLabelCss = colorizeBg ? "text-black" : "text-red";
        String redCellCss = colorizeBg ? "cell-red" : null;
        String blackCellLabelCss = colorizeBg ? "text-white" : "text-black";
        String blackCellCss = colorizeBg ? "cell-black" : null;
        Color highlightColor = colorizeBg ? Color.WHITE : Color.GRAY;

        List<CellData> redCells =
            IntStream.range(1, redCellsNumber + 1)
                     .mapToObj(i -> new CellData("r" + i, String.valueOf(i), redCellLabelCss, redCellCss, highlightColor))
                     .collect(Collectors.toList());
        Collections.reverse(redCells);
        List<CellData> blackCells =
            IntStream.range(1, blackCellsNumber + 1)
                     .mapToObj(i -> new CellData("b" + i, String.valueOf(i), blackCellLabelCss, blackCellCss, highlightColor))
                     .collect(Collectors.toList());
        cells = new ArrayList<CellData>();
        for (int i = 0; i < redCellsNumber; i++) {
            cells.add(blackCells.get(i));
            cells.add(redCells.get(i));
        }
        if (blackCellsNumber > redCellsNumber) {
            cells.add(blackCells.get(blackCellsNumber - 1));
        }
        currentIndex = -1;
        return new ArrayList<>(cells);
    }

    @Override
    public CellData nextCell() {
        return cells.get(++currentIndex);
    }

    @Override
    public CellData currentCell() {
        return cells.get(currentIndex);
    }

    @Override
    public boolean hasMoreElements() {
        return currentIndex < cells.size() - 1;
    }
}
