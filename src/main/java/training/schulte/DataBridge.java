package training.schulte;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javafx.scene.Scene;
import training.schulte.manager.SettingManager;

/**
 * Data bridge for controllers.
 * @author Alexey Chalov
 */
public final class DataBridge {

    private SchulteTable application;
    private static DataBridge instance = new DataBridge();

    private static final String BUNDLE = "bundle/strings";

    /**
     * Constructor.
     */
    private DataBridge() {
    }

    /**
     * Returns instance of data bridge.
     * @return {@link DataBridge} instance
     */
    public static DataBridge instance() {
        return instance;
    }

    /**
     * Returns application level resource bundle.
     * @return {@link ResourceBundle} instance
     */
    public ResourceBundle getResourceBundle() {
        return ResourceBundle.getBundle(BUNDLE, SettingManager.instance().getLocale(), new Utf8Control());
    }

    /**
     * Returns application level resource bundle for particular locale.
     * Used only to show alert, when database file is locked by unknown process.
     * @param locale {@link Locale} instance
     * @return {@link ResourceBundle} instance
     */
    public static ResourceBundle getResourceBundleLocale(Locale locale) {
        return ResourceBundle.getBundle(BUNDLE, locale, new Utf8Control());
    }

    /**
     * Sets application main class instance.
     * @param application {@link SchulteTable} instance
     */
    void setApplication(SchulteTable application) {
        this.application = application;
    }

    /**
     * Returns application main class instance.
     * @return {@link SchulteTable} instance
     */
    public SchulteTable getApplication() {
        return application;
    }

    /**
     * Returns current scene.
     * @return {@link Scene} instance
     */
    public Scene getScene() {
        return application.getStage().getScene();
    }

    /**
     * {@link Control} implementation for resource bundles in UTF-8 encoding.
     * @author Alexey Chalov
     */
    private static class Utf8Control extends Control {

        @Override
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
                                        throws IllegalAccessException, InstantiationException, IOException {
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");
            ResourceBundle bundle = null;
            InputStream stream = null;
            if (reload) {
                URL url = loader.getResource(resourceName);
                if (url != null) {
                    URLConnection connection = url.openConnection();
                    if (connection != null) {
                        connection.setUseCaches(false);
                        stream = connection.getInputStream();
                    }
                }
            } else {
                stream = loader.getResourceAsStream(resourceName);
            }
            if (stream != null) {
                try {
                    bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
                } finally {
                    stream.close();
                }
            }
            return bundle;
        }
    }
}
