package training.schulte.model;

/**
 * Time intervals enumeration for filtering game results.
 * @author Alexey Chalov
 */
public enum Interval {

    TODAY, FOR_WEEK, FOR_MONTH, ALL_TIME;
}
