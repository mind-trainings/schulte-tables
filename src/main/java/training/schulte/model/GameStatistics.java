package training.schulte.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Game statistics container.
 * @author Alexey Chalov
 */
@Getter
@RequiredArgsConstructor
public class GameStatistics {

    private final int minTimeMillis;
    private final int maxTimeMillis;
    private final int slowerGames;
    private final int totalGames;
}
