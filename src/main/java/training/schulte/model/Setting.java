package training.schulte.model;

/**
 * Settings enumeration.
 * @author Alexey Chalov
 */
public enum Setting {

    /* user settings */
    LOCALE, GAME_TYPE, GAME_SIZE_WIDTH, GAME_SIZE_HEIGHT, VOLUME_LEVEL, VOLUME_ON,

    /* system settings */
    MAX_RANK, NUMERIC_GAME_MIN_SIZE, NUMERIC_GAME_MAX_SIZE;
}
