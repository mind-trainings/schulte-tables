package training.schulte.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Rank holder for latest completed game.
 * @author Alexey Chalov
 */
@Getter
@RequiredArgsConstructor
public class GameRank {

    private final int rank;
    private final int time;
    private final int minTime;
    private final int maxRank;
    private final double fastPercent;
}
