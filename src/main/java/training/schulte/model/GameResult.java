package training.schulte.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import static training.schulte.util.GameUtil.formatTime;

/**
 * Completed games result holder.
 * @author Alexey Chalov
 */
@Getter
@RequiredArgsConstructor
public class GameResult {

    private final GameSize gameSize;
    private final int completedGames;
    private final int bestTimeMillis;
    private final int averageTimeMillis;

    /**
     * Returns display game size.
     * @return display game size.
     */
    public String getDisplayGameSize() {
        return gameSize.displayString();
    }

    /**
     * Returns best time formatted string.
     * @return best time formatted string
     */
    public String getBestTime() {
        return formatTime(bestTimeMillis);
    }

    /**
     * Returns average time formatted string.
     * @return average time formatted string
     */
    public String getAverageTime() {
        return formatTime(averageTimeMillis);
    }
}
