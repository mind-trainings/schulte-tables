package training.schulte.model;

/**
 * Game types enumeration.
 * @author Alexey Chalov
 */
public enum GameType {

    CLASSICAL_NUMERIC,
    BLACK_RED_NUMERIC;
}
