package training.schulte.model;

import javafx.scene.paint.Color;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Represents cell data for rendering on game pane.
 * @author Alexey Chalov
 */
@Getter
@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CellData {

    @EqualsAndHashCode.Include
    private final String value;
    private final String displayValue;
    private final String labelCssClass;
    private final String cellCssClass;
    private final Color tipHighlightColor;
}
