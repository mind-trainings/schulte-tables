package training.schulte.model.setting;

/**
 * Settings enumeration for black-red numeric game type.
 * @author Alexey Chalov
 */
public enum BlackRedNumericSetting {

    COLORIZE_BACKGROUND;
}
