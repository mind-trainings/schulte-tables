package training.schulte.model.setting;

/**
 * Settings enumeration for classical numeric game type.
 * @author Alexey Chalov
 */
public enum ClassicalNumericSetting {

    COUNTDOWN;
}
