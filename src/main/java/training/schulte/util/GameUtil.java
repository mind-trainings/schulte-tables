package training.schulte.util;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Utility class.
 * @author Alexey Chalov
 */
public final class GameUtil {

    public static final Locale LOCALE_RU = new Locale("ru", "RU");
    public static final Locale LOCALE_UK = Locale.UK;
    public static final Set<Locale> SUPPORTED_LOCALES = Arrays.stream(new Locale[] {LOCALE_UK, LOCALE_RU})
                                                              .collect(Collectors.toSet());

    public static final String DATA_DIR =
          System.getProperty("user.home") + System.getProperty("file.separator")
        + "mind-trainings" + System.getProperty("file.separator") + "schulte-tables" + System.getProperty("file.separator");
    public static final String DB_VERSION_FILE = DATA_DIR + "db.version";
    public static final String DB_FILE = DATA_DIR + "schulte-tables";
    public static final String DB_FILE_FULL = DB_FILE + ".mv.db";

    private static final ClassLoader CLASS_LOADER = GameUtil.class.getClassLoader();

    /**
     * Constructor.
     */
    private GameUtil() {
    }

    /**
     * Tries to guess locale using default one.
     * @return {@link Locale} instance
     */
    public static Locale guessLocale() {
        return Locale.getDefault().getLanguage().equals(LOCALE_UK.getLanguage()) ? LOCALE_UK : LOCALE_RU;
    }

    /**
     * Formats input time in milliseconds to human readable value.
     * @param time time in milliseconds
     * @return formatted time string
     */
    public static String formatTime(Number time) {
        long rounded = Math.round(time.intValue() / 1000d);
        long seconds = rounded % 60;
        return rounded / 60 + ":" + (seconds < 10 ? "0" : "") + seconds;
    }

    /**
     * Formats double value for corresponding locale.
     * @param locale {@link Locale} instance
     * @param value value to format
     * @return formatted string
     */
    public static String formatDouble(Locale locale, double value) {
        return new DecimalFormat("##.##", new DecimalFormatSymbols(locale)).format(value);
    }

    /**
     * Returns true, if passed {@link LocalDateTime} has the same day as current one, false otherwise.
     * @param dateTime {@link LocalDateTime} instance
     * @return boolean
     */
    public static boolean isCurrentDay(LocalDateTime dateTime) {
        return dateTime != null ? getStartDayDate().equals(dateTime.truncatedTo(ChronoUnit.DAYS)) : false;
    }

    /**
     * Returns start day date.
     * @return {@link LocalDateTime} instance
     */
    public static LocalDateTime getStartDayDate() {
        return LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
    }

    /**
     * Returns week ago date.
     * @return {@link LocalDateTime} instance
     */
    public static LocalDateTime getWeekAgoDate() {
        return LocalDateTime.now().minusWeeks(1).truncatedTo(ChronoUnit.DAYS);
    }

    /**
     * Returns month ago date.
     * @return {@link LocalDateTime} instance
     */
    public static LocalDateTime getMonthAgoDate() {
        return LocalDateTime.now().minus(1, ChronoUnit.MONTHS).truncatedTo(ChronoUnit.DAYS);
    }

    /**
     * Returns URL for resource at path.
     * @param path resource path
     * @return resource {@link URL}
     */
    public static URL getClassPathResourceUrl(String path) {
        return CLASS_LOADER.getResource(path);
    }
}
