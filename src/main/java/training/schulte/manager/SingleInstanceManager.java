package training.schulte.manager;

import static training.schulte.util.GameUtil.DB_FILE_FULL;
import static training.schulte.util.GameUtil.guessLocale;

import java.io.File;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.nio.channels.FileLock;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.stream.Stream;

import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ObjectMessage;
import org.jgroups.Receiver;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import training.schulte.DataBridge;

/**
 * Single application instance manager.
 * @author Alexey Chalov
 */
public final class SingleInstanceManager {

    private final boolean dbFileLocked;
    private JChannel channel;

    private static SingleInstanceManager instance = new SingleInstanceManager();

    private static final String ACTIVATE_WINDOW_MSG = "activateWindow";
    private static final String PROGRAM_EXIT_MSG = "exit";

    /**
     * Constructor.
     * Checks whether database file is locked (i.e. application is running or something holds lock on database),
     * and if so closes current application instance or forces to show alert message.
     */
    @SneakyThrows
    private SingleInstanceManager() {
        File dbFile = new File(DB_FILE_FULL);
        if (dbFile.exists()) {
            try (RandomAccessFile raf = new RandomAccessFile(dbFile, "rw");
                FileLock lock = raf.getChannel().tryLock(0, Long.MAX_VALUE, true)) {
                dbFileLocked = lock == null;
            }
        } else {
            dbFileLocked = false;
        }
        channel = new JChannel();
        channel = channel.connect("SchulteTables_" + InetAddress.getLocalHost().getHostName());
        if (dbFileLocked) {
            channel.setReceiver(new SubsequentInstanceReceiver());
        } else {
            channel.setReceiver(new FirstInstanceReceiver());
        }
    }

    /**
     * Returns instance of {@link SingleInstanceManager}.
     * @return {@link SingleInstanceManager} instance
     */
    public static SingleInstanceManager instance() {
        return instance;
    }

    /**
     * Performs necessary actions as closing subsequent application instances via message communication
     * and activating first application window, or showing alert dialog in case of database file locked by unknown process.
     * @return true
     */
    public boolean performActions() {
        if (dbFileLocked) {
            if (channel.getView().getMembers().size() > 1) {
                /* send message to activate first application instance window */
                sendMessage(ACTIVATE_WINDOW_MSG);
            } else {
                /* database file locked by unknown process => show alert */
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        ResourceBundle bundle = DataBridge.getResourceBundleLocale(guessLocale());
                        String fileName = DB_FILE_FULL.substring(DB_FILE_FULL.lastIndexOf(System.getProperty("file.separator")) + 1);
                        Alert alert = new Alert(AlertType.NONE, String.format(bundle.getString("startError.alertContent"), fileName), ButtonType.OK);
                        alert.setHeaderText(null);
                        alert.setTitle(bundle.getString("startError.alertTitle"));
                        alert.showAndWait();
                        /* terminate other running application instances */
                        sendMessage(PROGRAM_EXIT_MSG);
                        /* terminate current application instance */
                        sendMessage(PROGRAM_EXIT_MSG, channel.getAddress());
                    }
                });
            }
        }
        return !dbFileLocked;
    }

    /**
     * Closes JGroups channel.
     */
    public void closeChannel() {
        channel.close();
    }

    /**
     * Sends message to recipient list (multicasts it to all members in cluster except for current one) if
     * <code>addresses</code> parameter is null or empty, otherwise addresses array for message destinations is used.
     * @param message message to send
     * @param addresses members this message to send
     */
    private void sendMessage(String message, Address... addresses) {
        Stream<Address> addressesStream;
        if (addresses == null || addresses.length == 0) {
            addressesStream = channel.getView().getMembers().stream()
                                     .filter(m -> !m.equals(channel.getAddress()));
        } else {
            addressesStream = Arrays.stream(addresses);
        }
        addressesStream.forEach(m -> sendMessageInternal(message, m));
    }

    /**
     * Sends message to particular destination.
     * @param message message to send
     * @param address {@link Address} instance
     */
    @SneakyThrows
    private void sendMessageInternal(String message, Address address) {
        Message msg = new ObjectMessage();
        msg.setObject(message);
        msg.setDest(address);
        channel.send(msg);
    }

    /**
     * Message receiver for first application instance.
     * @author Alexey Chalov
     */
    private class FirstInstanceReceiver implements Receiver {

        @Override
        public void receive(Message msg) {
            if (ACTIVATE_WINDOW_MSG.equals(((ObjectMessage) msg).<String>getObject())) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        Stage stage = DataBridge.instance().getApplication().getStage();
                        stage.setIconified(false);
                        stage.toFront();
                    }
                });

                /* reply with message to exit another process */
                sendMessage(PROGRAM_EXIT_MSG, msg.getSrc());
            }
        }
    }

    /**
     * Message receiver for subsequent application instances.
     * @author Alexey Chalov
     */
    private class SubsequentInstanceReceiver implements Receiver {

        @Override
        public void receive(Message msg) {
            if (PROGRAM_EXIT_MSG.equals(((ObjectMessage) msg).<String>getObject())) {
                closeChannel();
                System.exit(0);
            }
        }
    }
}
