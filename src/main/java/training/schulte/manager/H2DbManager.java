package training.schulte.manager;

import static training.schulte.util.GameUtil.getStartDayDate;
import static training.schulte.util.GameUtil.getMonthAgoDate;
import static training.schulte.util.GameUtil.getWeekAgoDate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import training.schulte.model.GameResult;
import training.schulte.model.GameSize;
import training.schulte.model.GameStatistics;
import training.schulte.model.GameType;
import training.schulte.model.Interval;
import training.schulte.model.Setting;
import training.schulte.util.GameUtil;

/**
 * H2 database manager.
 * @author Alexey Chalov
 */
final class H2DbManager {

    private static H2DbManager dbManager = initDb();
    private static Connection connection;

    private static final int DB_VERSION = 3;

    /**
     * Constructor.
     */
    private H2DbManager() {
    }

    /**
     * Returns {@link H2DbManager} instance.
     * @return {@link H2DbManager} instance
     */
    public static H2DbManager instance() {
        return dbManager;
    }

    /**
     * Saves user setting.
     * @param setting setting constant
     * @param value setting value
     */
    @SneakyThrows
    public void saveSetting(Setting setting, Object value) {
        Connection conn = getConnection();
        String val = null;
        if (value.getClass() == Locale.class) {
            val = ((Locale) value).getLanguage() + "_" + ((Locale) value).getCountry();
        } else if (value.getClass() == GameType.class) {
            val = ((GameType) value).name();
        } else if (value.getClass() == Integer.class) {
            val = String.valueOf(value);
        }
        String query =
              "update setting"
            + "   set value = ?"
            + " where mnemo = ?";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, val);
            ps.setString(2, setting.name());
            ps.executeUpdate();
        }
    }

    /**
     * Returns user or system setting.
     * @param <T> return type
     * @param setting setting constant
     * @param targetClass class to cast result to
     * @return setting value
     */
    @SneakyThrows
    @SuppressWarnings("unchecked")
    public <T> T getSetting(Setting setting, Class<T> targetClass) {
        Connection conn = getConnection();
        Object result = null;
        String query =
              "select value"
            + "  from setting"
            + " where mnemo = ?";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, setting.name());
            ResultSet rs = ps.executeQuery();
            rs.next();
            String val = rs.getString(1);
            if (val != null && !val.trim().isEmpty()) {
                if (targetClass == Locale.class) {
                    String[] localeData = val.split("_");
                    result = new Locale(localeData[0], localeData[1]);
                } else if (targetClass == GameType.class) {
                    result = GameType.valueOf(val);
                } else if (targetClass == Integer.class) {
                    result = rs.getInt(1);
                }
            }
        }
        return (T) result;
    }

    /**
     * Saves setting for particular game type.
     * @param gameType {@link GameType} constant
     * @param settingName setting name
     * @param settingValue setting value
     */
    @SneakyThrows
    public void saveGameSetting(GameType gameType, String settingName, String settingValue) {
        Connection conn = getConnection();
        String query =
              "update game_setting"
            + "   set setting_value = ?"
            + " where game_type = ?"
            + "   and setting_name = ?";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, settingValue);
            ps.setString(2, gameType.name());
            ps.setString(3, settingName);
            ps.executeUpdate();
        }
    }

    /**
     * Returns setting for particular game type.
     * @param gameType {@link GameType} constant
     * @param settingName setting name
     * @return setting value
     */
    @SneakyThrows
    public String getGameSetting(GameType gameType, String settingName) {
        Connection conn = getConnection();
        String query =
              "select setting_value"
            + "  from game_setting"
            + " where game_type = ?"
            + "   and setting_name = ?";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, gameType.name());
            ps.setString(2, settingName);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString(1);
        }
    }

    /**
     * Returns settings map for particular game type.
     * @param gameType {@link GameType} constant
     * @return settings map
     */
    @SneakyThrows
    public Map<String, String> getGameSettings(GameType gameType) {
        Connection conn = getConnection();
        Map<String, String> result = new HashMap<>();
        String query =
              "select setting_name, setting_value"
            + "  from game_setting"
            + " where game_type = ?";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, gameType.name());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                result.put(rs.getString(1), rs.getString(2));
            }
            return result;
        }
    }

    /**
     * Saves game result.
     * @param gameType {@link GameType} constant
     * @param gameSize {@link GameSize} instance
     * @param playTime play time
     */
    @SneakyThrows
    public void saveGameResult(GameType gameType, GameSize gameSize, int playTime) {
        Connection conn = getConnection();
        String query =
              "insert into game_result(game_type, game_size_width, game_size_height, play_time) "
            + "values(?, ?, ?, ?)";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, gameType.name());
            ps.setInt(2, gameSize.getWidth());
            ps.setInt(3, gameSize.getHeight());
            ps.setInt(4, playTime);
            ps.executeUpdate();
        }
    }

    /**
     * Returns true, if no games were played yet, false otherwise.
     * @return boolean
     */
    @SneakyThrows
    public boolean isGameResultsEmpty() {
        Connection conn = getConnection();
        String query =
              "select null"
            + "  from game_result"
            + " limit 1";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            return !rs.next();
        }
    }

    /**
     * Returns list of completed game types.
     * @return list of completed game types
     */
    @SneakyThrows
    public List<GameType> getCompletedGameTypes() {
        Connection conn = getConnection();
        List<GameType> result = new ArrayList<>();
        String query =
              "select distinct game_type"
            + "  from game_result";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                result.add(GameType.valueOf(rs.getString(1)));
            }
        }
        Collections.sort(result);
        return result;
    }

    /**
     * Returns date of latest played game.
     * @param gameType game type
     * @return date of latest played game
     */
    @SneakyThrows
    public LocalDateTime getLatestGamePlayTime(GameType gameType) {
        Connection conn = getConnection();
        String query =
              "select complete_date"
            + "  from game_result"
            + " where game_type = ?"
            + " order by complete_date desc"
            + " limit 1";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, gameType.name());
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getTimestamp(1).toLocalDateTime();
        }
    }

    /**
     * Returns list or game results for particular game type, taking into account selected time interval.
     * @param gameType {@link GameType} constant
     * @param interval {@link Interval} constant
     * @return list of game results
     */
    @SneakyThrows
    public List<GameResult> getGameResults(GameType gameType, Interval interval) {
        LocalDateTime cutOffDate = null;
        switch (interval) {
            case TODAY:
                cutOffDate = getStartDayDate();
                break;
            case FOR_WEEK:
                cutOffDate = getWeekAgoDate();
                break;
            case FOR_MONTH:
                cutOffDate = getMonthAgoDate();
                break;
            case ALL_TIME:
                break;
            default:
                break;
        }

        List<GameResult> results = new ArrayList<>();
        Connection conn = getConnection();
        String query =
              "select game_size_width, game_size_height, min(play_time) min_play_time,"
            + "       avg(play_time) avg_play_time, count(1) games_played"
            + "  from game_result"
            + " where game_type = ?"
            + "   and complete_date >= ?"
            + " group by game_size_width, game_size_height"
            + " order by game_size_width, game_size_height";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, gameType.name());
            ps.setTimestamp(2, new Timestamp(cutOffDate != null ? cutOffDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() : 0));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(new GameResult(
                    new GameSize(rs.getInt("game_size_width"), rs.getInt("game_size_height")),
                    rs.getInt("games_played"), rs.getInt("min_play_time"), rs.getInt("avg_play_time")
                ));
            }
        }
        return results;
    }

    /**
     * Returns game statistics for particular game type and size.
     * @param gameType {@link GameType} constant
     * @param gameSize {@link GameSize} instance
     * @param playTime current game play time
     * @return {@link GameStatistics} instance
     */
    @SneakyThrows
    public GameStatistics getGameStatistics(GameType gameType, GameSize gameSize, int playTime) {
        Connection conn = getConnection();
        String query =
              "select min(play_time) min_play_time, max(play_time) max_play_time,"
            + "       sum(case when play_time > ? then 1 else 0 end) slower_games, count(1) total_games"
            + "  from game_result"
            + " where game_type = ?"
            + "   and game_size_width = ?"
            + "   and game_size_height = ?";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setInt(1, playTime);
            ps.setString(2, gameType.name());
            ps.setInt(3, gameSize.getWidth());
            ps.setInt(4, gameSize.getHeight());
            ResultSet rs = ps.executeQuery();
            rs.next();
            return new GameStatistics(
                rs.getInt("min_play_time"), rs.getInt("max_play_time"), rs.getInt("slower_games"), rs.getInt("total_games")
            );
        }
    }

    /**
     * Initializes {@link H2DbManager} instance.
     * @return {@link H2DbManager} instance
     */
    @SneakyThrows
    private static H2DbManager initDb() {
        File dataDir = new File(GameUtil.DATA_DIR);
        if (!dataDir.exists()) {
            dataDir.mkdir();
        }
        boolean runLiquibase = true;
        File dbVersionFile = new File(GameUtil.DB_VERSION_FILE);
        if (dbVersionFile.exists()) {
            try (BufferedReader br = new BufferedReader(new FileReader(dbVersionFile))) {
                int dbVersion = Integer.parseInt(br.readLine());
                runLiquibase = dbVersion != DB_VERSION;
            }
        }
        if (runLiquibase || !dbVersionFile.exists()) {
            new File(dbVersionFile.getParent()).mkdirs();
            try (FileWriter fw = new FileWriter(dbVersionFile)) {
                fw.write(String.valueOf(DB_VERSION));
            }
            liquibaseUpdate();
        }
        return new H2DbManager();
    }

    /**
     * Setups database structure.
     */
    @SneakyThrows
    private static void liquibaseUpdate() {
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(getConnection()));
        Liquibase liquibase = new Liquibase(
            "/liquibase/changeset.sql", new ClassLoaderResourceAccessor(), database
        );
        liquibase.clearCheckSums();
        liquibase.update((String) null);
        liquibase.close();
    }

    /**
     * Initializes database connection.
     * @return {@link Connection} object
     */
    @SneakyThrows
    private static Connection getConnection() {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection("jdbc:h2:file:" + GameUtil.DB_FILE + ";TRACE_LEVEL_FILE=0");
        }
        return connection;
    }
}
