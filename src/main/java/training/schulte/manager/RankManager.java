package training.schulte.manager;

import static training.schulte.model.Setting.MAX_RANK;
import static training.schulte.util.GameUtil.getMonthAgoDate;
import static training.schulte.util.GameUtil.getStartDayDate;
import static training.schulte.util.GameUtil.getWeekAgoDate;
import static training.schulte.util.GameUtil.isCurrentDay;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import training.schulte.model.GameRank;
import training.schulte.model.GameResult;
import training.schulte.model.GameSize;
import training.schulte.model.GameStatistics;
import training.schulte.model.GameType;
import training.schulte.model.Interval;

/**
 * User rank manager.
 * @author Alexey Chalov
 */
public final class RankManager {

    /* filter values */
    private GameType gameType;
    private Interval interval;
    private LocalDateTime intervalSetupDate;

    /* latest completed game rank */
    private GameRank gameRank;

    private H2DbManager dbManager;
    private static RankManager rankManager = new RankManager();

    /**
     * Constructor.
     */
    private RankManager() {
        dbManager = H2DbManager.instance();
    }

    /**
     * Returns instance of rank manager.
     * @return {@link RankManager} instance
     */
    public static RankManager instance() {
        return rankManager;
    }

    /**
     * Returns latest game rank.
     * @return {@link GameRank} instance
     */
    public GameRank getGameRank() {
        return gameRank;
    }

    /**
     * Saves game result.
     * @param gameType {@link GameType} constant
     * @param gameSize {@link GameSize} instance
     * @param playTime play time
     */
    public void saveResult(GameType gameType, GameSize gameSize, Number playTime) {
        int gamePlayTime = playTime.intValue();
        setupGameRank(gameType, gameSize, gamePlayTime);
        dbManager.saveGameResult(gameType, gameSize, gamePlayTime);
    }

    /**
     * Returns true, if no game results is stored yet, false otherwise.
     * @return boolean
     */
    public boolean isResultsEmpty() {
        return dbManager.isGameResultsEmpty();
    }

    /**
     * Returns game type for filtering results.
     * @return {@link GameType} instance
     */
    public GameType getGameType() {
        return gameType != null ? gameType : getCompletedGameTypes().get(0);
    }

    /**
     * Set game type filter.
     * @param gameType {@link GameType} instance
     */
    public void setGameType(GameType gameType) {
        this.gameType = gameType;
        this.interval = null;
    }

    /**
     * Returns interval for filtering results.
     * @return {@link Interval} instance
     */
    public Interval getInterval() {
        if (interval == null || !isCurrentDay(intervalSetupDate)) {
            interval = getAllowedIntervals().get(0);
            intervalSetupDate = LocalDateTime.now();
        }
        return interval;
    }

    /**
     * Sets interval filter.
     * @param interval {@link Interval} instance
     */
    public void setInterval(Interval interval) {
        this.interval = interval;
        intervalSetupDate = LocalDateTime.now();
    }

    /**
     * Returns list of completed game types.
     * @return list of completed game types
     */
    public List<GameType> getCompletedGameTypes() {
        return dbManager.getCompletedGameTypes();
    }

    /**
     * Returns list of allowed intervals, regarding latest game play time.
     * @return list of allowed intervals
     */
    public List<Interval> getAllowedIntervals() {
        LocalDateTime latestGameDate = dbManager.getLatestGamePlayTime(getGameType());
        List<Interval> result = new ArrayList<Interval>();
        if (latestGameDate.isAfter(getStartDayDate())) {
            result.add(Interval.TODAY);
        }
        if (latestGameDate.isAfter(getWeekAgoDate())) {
            result.add(Interval.FOR_WEEK);
        }
        if (latestGameDate.isAfter(getMonthAgoDate())) {
            result.add(Interval.FOR_MONTH);
        }
        result.add(Interval.ALL_TIME);
        return result;
    }

    /**
     * Returns filtered game results.
     * @return filtered game results
     */
    public List<GameResult> getGameResults() {
        return dbManager.getGameResults(getGameType(), getInterval());
    }

    /**
     * Sets up game rank on game completion.
     * @param gameType {@link GameType} constant
     * @param gameSize {@link GameSize} instance
     * @param playTime game play time
     */
    private void setupGameRank(GameType gameType, GameSize gameSize, int playTime) {
        int maxRank = dbManager.getSetting(MAX_RANK, Integer.class);
        int rank;
        GameStatistics statistics = dbManager.getGameStatistics(gameType, gameSize, playTime);
        double fastPercent = 100 * (
            statistics.getTotalGames() == 0 ? 1 : statistics.getSlowerGames() / (double) statistics.getTotalGames()
        );
        rank = Math.max((int) Math.ceil(fastPercent / 100 * maxRank), 1);
        gameRank = new GameRank(
            rank, playTime,
            statistics.getTotalGames() == 0 || playTime < statistics.getMinTimeMillis() ? playTime : statistics.getMinTimeMillis(),
            maxRank, fastPercent
        );
    }
}
