package training.schulte.manager;

import static training.schulte.model.Setting.GAME_SIZE_HEIGHT;
import static training.schulte.model.Setting.GAME_SIZE_WIDTH;
import static training.schulte.model.Setting.GAME_TYPE;
import static training.schulte.model.Setting.LOCALE;
import static training.schulte.model.Setting.NUMERIC_GAME_MAX_SIZE;
import static training.schulte.model.Setting.NUMERIC_GAME_MIN_SIZE;
import static training.schulte.model.Setting.VOLUME_LEVEL;
import static training.schulte.model.Setting.VOLUME_ON;
import static training.schulte.util.GameUtil.SUPPORTED_LOCALES;
import static training.schulte.util.GameUtil.guessLocale;

import java.util.Locale;
import java.util.Map;

import training.schulte.model.GameSize;
import training.schulte.model.GameType;
import training.schulte.model.Setting;

/**
 * User settings manager.
 * @author Alexey Chalov
 */
public final class SettingManager {

    private H2DbManager dbManager;
    private static SettingManager settingManager = new SettingManager();

    /**
     * Constructor.
     */
    private SettingManager() {
        dbManager = H2DbManager.instance();
    }

    /**
     * Returns instance of setting manager.
     * @return {@link SettingManager} instance
     */
    public static SettingManager instance() {
        return settingManager;
    }

    /**
     * Returns true, if volume is on, false if mute.
     * @return boolean
     */
    public boolean isVolumeOn() {
        return dbManager.getSetting(VOLUME_ON, Integer.class) == 1;
    }

    /**
     * Saves {@link Setting#VOLUME_ON} value.
     * @param on if true, volume is on, mute otherwise
     */
    public void saveVolume(boolean on) {
        dbManager.saveSetting(VOLUME_ON, on ? 1 : 0);
    }

    /**
     * Returns volume level.
     * @return volume level
     */
    public int getVolumeLevel() {
        return dbManager.getSetting(VOLUME_LEVEL, Integer.class);
    }

    /**
     * Saves volume level.
     * @param level level value
     */
    public void saveVolumeLevel(int level) {
        dbManager.saveSetting(VOLUME_LEVEL, level);
    }

    /**
     * Restores latest selected game type from settings and returns it.
     * @return latest selected game type
     */
    public GameType getGameType() {
        return dbManager.getSetting(GAME_TYPE, GameType.class);
    }

    /**
     * Saves game type.
     * @param gameType {@link GameType} instance
     */
    public void saveGameType(GameType gameType) {
        dbManager.saveSetting(GAME_TYPE, gameType);
    }

    /**
     * Returns latest played game size.
     * @return latest played game size
     */
    public GameSize getGameSize() {
        return new GameSize(
            dbManager.getSetting(GAME_SIZE_WIDTH, Integer.class),
            dbManager.getSetting(GAME_SIZE_HEIGHT, Integer.class)
        );
    }

    /**
     * Saves game size.
     * @param gameSize game size
     */
    public void saveGameSize(GameSize gameSize) {
        dbManager.saveSetting(GAME_SIZE_WIDTH, gameSize.getWidth());
        dbManager.saveSetting(GAME_SIZE_HEIGHT, gameSize.getHeight());
    }

    /**
     * Returns user selected locale.
     * @return {@link Locale} instance
     */
    public Locale getLocale() {
        Locale locale = dbManager.getSetting(LOCALE, Locale.class);
        if (SUPPORTED_LOCALES.contains(locale)) {
            return locale;
        }
        return guessLocale();
    }

    /**
     * Saves locale.
     * @param locale {@link Locale} instance
     */
    public void saveLocale(Locale locale) {
        if (SUPPORTED_LOCALES.contains(locale)) {
            dbManager.saveSetting(LOCALE, locale);
        }
    }

    /**
     * Returns numeric game minimum size (width or height).
     * @return numeric game minimum size (width or height)
     */
    public int getNumericGameMinSize() {
        return dbManager.getSetting(NUMERIC_GAME_MIN_SIZE, Integer.class);
    }

    /**
     * Returns numeric game maximum size (width or height).
     * @return numeric game maximum size (width or height)
     */
    public int getNumericGameMaxSize() {
        return dbManager.getSetting(NUMERIC_GAME_MAX_SIZE, Integer.class);
    }

    /**
     * Saves setting for particular game type.
     * @param gameType {@link GameType} constant
     * @param settingName setting name
     * @param settingValue setting value
     */
    public void saveGameSetting(GameType gameType, String settingName, String settingValue) {
        dbManager.saveGameSetting(gameType, settingName, settingValue);
    }

    /**
     * Returns setting for particular game type.
     * @param <T> return value type
     * @param gameType {@link GameType} constant
     * @param settingName setting name
     * @param type return type class
     * @return setting value
     */
    @SuppressWarnings("unchecked")
    public <T> T getGameSetting(GameType gameType, String settingName, Class<T> type) {
        String settingValue = dbManager.getGameSetting(gameType, settingName);
        Object result = null;
        if (type == Integer.class) {
            result = Integer.parseInt(settingValue);
        }
        return (T) result;
    }

    /**
     * Returns settings map for particular game type.
     * @param gameType {@link GameType} constant
     * @return settings map
     */
    public Map<String, String> getGameSettings(GameType gameType) {
        return dbManager.getGameSettings(gameType);
    }
}
