package training.schulte.controller;

import static training.schulte.util.GameUtil.getClassPathResourceUrl;
import static training.schulte.util.GameUtil.formatDouble;
import static training.schulte.util.GameUtil.formatTime;

import java.util.ResourceBundle;
import java.util.stream.IntStream;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import training.schulte.DataBridge;
import training.schulte.manager.RankManager;
import training.schulte.manager.SettingManager;
import training.schulte.model.GameRank;

/**
 * GameResult.fxml controller.
 * @author Alexey Chalov
 */
public class GameResultController {

    @FXML
    private Label statusLabel;
    @FXML
    private Label gameResultLabel;
    @FXML
    private Label minTimeLabel;
    @FXML
    private Label fastPercentLabel;
    @FXML
    private Label rankLabel;
    @FXML
    private HBox rankBox;

    private static final DataBridge DATA_BRIDGE = DataBridge.instance();
    private static final SettingManager SETTING_MANAGER = SettingManager.instance();
    private static final RankManager RANK_MANAGER = RankManager.instance();

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        GameRank gameRank = RANK_MANAGER.getGameRank();
        ResourceBundle bundle = DATA_BRIDGE.getResourceBundle();
        boolean firstPlace = gameRank.getFastPercent() == 100d;
        statusLabel.setText(bundle.getString(firstPlace ? "gameResult.newRecord" : "gameResult.congratulations"));

        if (firstPlace) {
            DropShadow shadow = new DropShadow();
            shadow.setColor(Color.GRAY);
            shadow.setSpread(0);
            shadow.setBlurType(BlurType.GAUSSIAN);

            Timeline shadowAnimation = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(shadow .radiusProperty(), 0d)),
                new KeyFrame(Duration.seconds(0.8), new KeyValue(shadow.radiusProperty(), 10d)),
                new KeyFrame(Duration.seconds(1.6), new KeyValue(shadow.radiusProperty(), 0d))
            );
            shadowAnimation.setCycleCount(Animation.INDEFINITE);
            statusLabel.setEffect(shadow);
            shadowAnimation.play();
        }

        gameResultLabel.setText(String.format(
            bundle.getString("gameResult.title"),
            SETTING_MANAGER.getGameSize().displayString(),
            formatTime(gameRank.getTime())
        ));
        minTimeLabel.setText(String.format(
            bundle.getString("gameResult.minTime"),
            formatTime(Math.min(gameRank.getTime(), gameRank.getMinTime()))
        ));
        fastPercentLabel.setText(String.format(
            bundle.getString("gameResult.fastPercent"),
            formatDouble(SETTING_MANAGER.getLocale(), gameRank.getFastPercent())
        ));
        rankLabel.setText(String.format(
            bundle.getString("gameResult.rank"), gameRank.getRank(), gameRank.getMaxRank()
        ));
        IntStream.range(1, gameRank.getRank() + 1).forEach(
            i -> rankBox.getChildren().add(new ImageView(getClassPathResourceUrl("image/crown.png").toExternalForm()))
        );
    }
}
