package training.schulte.controller;

import static training.schulte.util.GameUtil.formatTime;
import static training.schulte.util.GameUtil.getClassPathResourceUrl;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.SneakyThrows;
import training.schulte.DataBridge;
import training.schulte.celldata.CellDataGenerator;
import training.schulte.manager.RankManager;
import training.schulte.manager.SettingManager;
import training.schulte.model.CellData;
import training.schulte.model.GameSize;
import training.schulte.model.GameType;

/**
 * Game.fxml controller.
 * @author Alexey Chalov
 */
public class GameController {

    private static final DataBridge DATA_BRIDGE = DataBridge.instance();
    private static final SettingManager SETTING_MANAGER = SettingManager.instance();
    private static final RankManager RANK_MANAGER = RankManager.instance();

    @FXML
    private Label timerLabel;
    @FXML
    private Button restartButton;
    @FXML
    private Button volumeButton;
    @FXML
    private ImageView volumeImage;
    @FXML
    private GridPane gamePane;
    @FXML
    private HBox nextSearchCell;
    @FXML
    private Label nextSearchLabel;

    private Timeline timeline;

    private long startTime;

    private MediaPlayer okPlayer;
    private MediaPlayer errorPlayer;
    private MediaPlayer tadaPlayer;
    private MediaPlayer[] players;

    private Pane gameCompletedPane;
    private boolean gameCompleted;

    private GameType gameType;
    private GameSize gameSize;
    private CellData currentCell;
    private CellDataGenerator cellDataGenerator;
    private VBox volumeControl;

    /**
     * Initializer method.
     */
    @FXML
    @SneakyThrows
    private void initialize() {
        gameType = SETTING_MANAGER.getGameType();
        gameSize = SETTING_MANAGER.getGameSize();
        volumeControl = new FXMLLoader(getClassPathResourceUrl("view/Volume.fxml"), DATA_BRIDGE.getResourceBundle()).load();

        timeline = new Timeline(new KeyFrame(Duration.seconds(1), e -> setupTimerLabel(null)));
        timeline.setCycleCount(Animation.INDEFINITE);

        okPlayer = new MediaPlayer(new Media(getClassPathResourceUrl("sound/ok.mp3").toExternalForm()));
        errorPlayer = new MediaPlayer(new Media(getClassPathResourceUrl("sound/error.mp3").toExternalForm()));
        tadaPlayer = new MediaPlayer(new Media(getClassPathResourceUrl("sound/tada.mp3").toExternalForm()));
        players = new MediaPlayer[] {okPlayer, errorPlayer, tadaPlayer};

        setupVolume();
        setupGamePane();
        initGamePane();
        timeline.playFromStart();

        initTipFilter();
    }

    /**
     * Shows popup for volume setup.
     */
    @FXML
    private void showVolumePopup() {
        Stage stage = DATA_BRIDGE.getApplication().getStage();
        Popup volumePopup = new Popup();
        volumePopup.setAutoHide(true);

        Bounds boundsInScreen = volumeButton.localToScreen(volumeButton.getBoundsInLocal());
        volumePopup.setX(boundsInScreen.getMinX());
        volumePopup.setY(boundsInScreen.getMaxY() + 3);

        volumePopup.getContent().add(volumeControl);
        volumePopup.setOnAutoHide(e -> setupVolume());
        volumePopup.show(stage);
    }

    /**
     * Starts game again.
     */
    @FXML
    private void startAgain() {
        initGamePane();
        timeline.playFromStart();
    }

    /**
     * Main menu navigation.
     */
    @FXML
    private void mainMenu() {
        timeline.stop();
        DATA_BRIDGE.getApplication().buildStage();
    }

    /**
     * Sets up volume button image and players.
     */
    private void setupVolume() {
        boolean on = SETTING_MANAGER.isVolumeOn();
        int level = SETTING_MANAGER.getVolumeLevel();
        volumeImage.setImage(new Image(getClassPathResourceUrl("image/" + (on ? "speaker.png" : "mute.png")).toExternalForm()));
        Arrays.stream(players).forEach(p -> {
            p.setMute(!on);
            p.setVolume(level / 100d);
        });
    }

    /**
     * Sets up game pane with constraints.
     */
    private void setupGamePane() {
        IntStream.range(0, gameSize.getWidth()).forEach(h -> {
            ColumnConstraints cc = new ColumnConstraints();
            cc.setHalignment(HPos.CENTER);
            gamePane.getColumnConstraints().add(cc);
        });
        IntStream.range(0, gameSize.getHeight()).forEach(v -> {
            RowConstraints rc = new RowConstraints();
            rc.setValignment(VPos.CENTER);
            gamePane.getRowConstraints().add(rc);
        });
    }

    /**
     * Initializes game pane.
     */
    private void initGamePane() {
        gameCompleted = false;
        startTime = System.currentTimeMillis();

        cellDataGenerator = new CellDataGenerator(gameType, SETTING_MANAGER.getGameSettings(gameType));
        List<CellData> values = cellDataGenerator.generate(gameSize);
        currentCell = cellDataGenerator.nextCell();

        setupTimerLabel(null);

        restartButton.setDisable(true);

        int size = getGameAreaSize();
        int cellSize = Math.min(size / gameSize.getWidth(), size / gameSize.getHeight());

        Iterator<CellData> valuesIterator = values.iterator();

        double fontSizeCoefficient = getFontSizeCoefficient();

        IntStream.range(0, gameSize.getWidth()).forEach(h -> {
            IntStream.range(0, gameSize.getHeight()).forEach(v -> {
                CellData cellData = valuesIterator.next();
                HBox cell = createCell(cellSize, fontSizeCoefficient, cellData);
                cell.getStyleClass().add(cellData.getCellCssClass() != null ? cellData.getCellCssClass() : "default-grid-cell");
                if (h == 0) {
                    cell.getStyleClass().add("first-column");
                }
                if (v == 0) {
                    cell.getStyleClass().add("first-row");
                }
                gamePane.add(cell, h, v);
            });
        });
        ((HBox) gamePane.getParent()).getChildren().remove(gameCompletedPane);
    }

    /**
     * Creates game cell.
     * @param size cell size
     * @param fontSizeCoefficient font size correcting coefficient
     * @param cellData {@link CellData} instance
     * @return created cell
     */
    private HBox createCell(int size, double fontSizeCoefficient, CellData cellData) {
        HBox cell = new HBox();
        cell.setMinWidth(size);
        cell.setMinHeight(size);
        cell.setAlignment(Pos.CENTER);
        cell.setId(cellData.getValue());
        cell.setOnMousePressed(e -> {
            if (e.getClickCount() == 1) {
                selectCell(cellData.getValue());
            }
            e.consume();
        });
        Label label = new Label(String.valueOf(cellData.getDisplayValue()));
        label.setStyle("-fx-font-size: " + size * fontSizeCoefficient + " px;");
        if (cellData.getLabelCssClass() != null) {
            label.getStyleClass().add(cellData.getLabelCssClass());
        }
        cell.getChildren().add(label);
        return cell;
    }

    /**
     * Select cell handler.
     * @param cellDataValue value associated with selected cell
     */
    private void selectCell(String cellDataValue) {
        if (!gameCompleted) {
            if (cellDataValue.equals(cellDataGenerator.currentCell().getValue()) && !cellDataGenerator.hasMoreElements()) {
                long completeTime = System.currentTimeMillis();
                RANK_MANAGER.saveResult(
                    SETTING_MANAGER.getGameType(), SETTING_MANAGER.getGameSize(), completeTime - startTime
                );
                timeline.stop();
                restartButton.setDisable(false);
                setupTimerLabel(completeTime);
                playSound(tadaPlayer);
                showGameResultPane();
                gameCompleted = true;
                return;
            }
            if (cellDataValue.equals(cellDataGenerator.currentCell().getValue())) {
                currentCell = cellDataGenerator.nextCell();
                setupTimerLabel(null);
                playSound(okPlayer);
            } else {
                playSound(errorPlayer);
            }
        }
    }

    /**
     * Initializes tip event filter.
     */
    private void initTipFilter() {
        DATA_BRIDGE.getScene().addEventFilter(KeyEvent.KEY_PRESSED, (ke) -> {
            if (ke.getCode() == KeyCode.SPACE) {
                Label label = (Label) gamePane.getChildren().stream()
                                              .filter(node -> ((HBox) node).getId().contentEquals(currentCell.getValue()))
                                              .map(node ->  ((HBox) node).getChildren().get(0))
                                              .findFirst().get();
                DropShadow shadow = new DropShadow();
                shadow.setColor(currentCell.getTipHighlightColor());
                shadow.setSpread(0.5);
                shadow.setBlurType(BlurType.GAUSSIAN);

                Timeline shadowAnimation = new Timeline(
                    new KeyFrame(Duration.ZERO, new KeyValue(shadow.radiusProperty(), 0d)),
                    new KeyFrame(Duration.seconds(0.3), new KeyValue(shadow.radiusProperty(), 50d)),
                    new KeyFrame(Duration.seconds(0.6), new KeyValue(shadow.radiusProperty(), 0d))
                );
                shadowAnimation.setCycleCount(2);
                label.setEffect(shadow);
                shadowAnimation.setOnFinished(e -> label.setEffect(null));
                shadowAnimation.play();
                ke.consume();
            }
        });
    }

    /**
     * Plays sound.
     * @param player {@link MediaPlayer} instance
     */
    private void playSound(MediaPlayer player) {
        player.play();
        player.seek(Duration.ZERO);
    }

    /**
     * Returns game area size.
     * @return game area size
     */
    private int getGameAreaSize() {
        return (int) (((Pane) gamePane.getParent()).getPrefHeight() * 0.95);
    }

    /**
     * Returns font size correcting coefficient.
     * @return font size correcting coefficient
     */
    private double getFontSizeCoefficient() {
        int totalCells = gameSize.getWidth() * gameSize.getHeight();
        if (totalCells < 100) {
            return 0.7;
        }
        return 0.5;
    }

    /**
     * Sets up timer label.
     * @param completeTime game complete time, or null if game is in progress
     */
    private void setupTimerLabel(Long completeTime) {
        timerLabel.setText(String.format(
            DATA_BRIDGE.getResourceBundle().getString("game.timer"),
            formatTime(startTime == 0 ? 0 : (completeTime != null ? completeTime : System.currentTimeMillis()) - startTime)
        ));
        nextSearchCell.getStyleClass().clear();
        nextSearchCell.getStyleClass().add("border-black");
        nextSearchCell.getStyleClass().add(currentCell.getCellCssClass());

        nextSearchLabel.getStyleClass().clear();
        nextSearchLabel.getStyleClass().add("font-l-bold");
        nextSearchLabel.getStyleClass().add(currentCell.getLabelCssClass());
        nextSearchLabel.setText(currentCell.getDisplayValue());
    }

    /**
     * Shows game result pane on game completion.
     */
    @SneakyThrows
    private void showGameResultPane() {
        gamePane.getChildren().clear();
        gameCompletedPane = new FXMLLoader(getClassPathResourceUrl("view/GameResult.fxml"), DATA_BRIDGE.getResourceBundle()).load();
        ObservableList<Node> children = ((HBox) gamePane.getParent()).getChildren();
        children.add(gameCompletedPane);
    }
}
