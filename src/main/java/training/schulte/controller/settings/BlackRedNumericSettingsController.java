package training.schulte.controller.settings;

import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import training.schulte.manager.SettingManager;
import training.schulte.model.GameType;
import training.schulte.model.setting.BlackRedNumericSetting;

/**
 * {@link training.schulte.model.GameType#BLACK_RED_NUMERIC} settings controller.
 * @author Alexey Chalov
 */
public class BlackRedNumericSettingsController implements SettingsController {

    @FXML
    private RadioButton colorizeBgButton;
    @FXML
    private RadioButton colorizeTextButton;

    private static final SettingManager SETTING_MANAGER = SettingManager.instance();

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        boolean colorizeBg = SETTING_MANAGER.getGameSetting(
            GameType.BLACK_RED_NUMERIC, BlackRedNumericSetting.COLORIZE_BACKGROUND.name(), Integer.class
        ) == 1;
        colorizeBgButton.setSelected(colorizeBg);
        colorizeTextButton.setSelected(!colorizeBg);
    }

    @FXML
    @Override
    public void saveSettings() {
        SETTING_MANAGER.saveGameSetting(
            GameType.BLACK_RED_NUMERIC,
            BlackRedNumericSetting.COLORIZE_BACKGROUND.name(),
            String.valueOf(colorizeBgButton.isSelected() ? 1 : 0)
        );
        colorizeBgButton.getScene().getWindow().hide();
    }
}
