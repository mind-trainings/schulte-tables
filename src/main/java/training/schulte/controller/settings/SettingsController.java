package training.schulte.controller.settings;

/**
 * Base interface for game type settings.
 * @author Alexey Chalov
 */
interface SettingsController {

    /**
     * Saves custom settings.
     */
    void saveSettings();
}
