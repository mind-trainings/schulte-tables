package training.schulte.controller.settings;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import training.schulte.manager.SettingManager;
import training.schulte.model.GameType;
import training.schulte.model.setting.ClassicalNumericSetting;

/**
 * {@link training.schulte.model.GameType#CLASSICAL_NUMERIC} settings controller.
 * @author Alexey Chalov
 */
public class ClassicalNumericSettingsController implements SettingsController {

    @FXML
    private CheckBox countdownCheckBox;

    private static final SettingManager SETTING_MANAGER = SettingManager.instance();

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        countdownCheckBox.setSelected(
            SETTING_MANAGER.getGameSetting(GameType.CLASSICAL_NUMERIC, ClassicalNumericSetting.COUNTDOWN.name(), Integer.class) == 1
        );
    }

    @FXML
    @Override
    public void saveSettings() {
        SETTING_MANAGER.saveGameSetting(
            GameType.CLASSICAL_NUMERIC,
            ClassicalNumericSetting.COUNTDOWN.name(),
            String.valueOf(countdownCheckBox.isSelected() ? 1 : 0)
        );
        countdownCheckBox.getScene().getWindow().hide();
    }
}
