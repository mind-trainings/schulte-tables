package training.schulte.controller;

import static training.schulte.util.GameUtil.getClassPathResourceUrl;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import training.schulte.manager.SettingManager;

/**
 * Volume.fxml controller.
 * @author Alexey Chalov
 */
public class VolumeController {

    @FXML
    private ImageView volumeImage;
    @FXML
    private Label volumeLabel;
    @FXML
    private Slider volumeSlider;

    private Image onImage;
    private Image muteImage;

    private static final SettingManager SETTING_MANAGER = SettingManager.instance();

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        onImage = new Image(getClassPathResourceUrl("image/speaker.png").toExternalForm());
        muteImage = new Image(getClassPathResourceUrl("image/mute.png").toExternalForm());
        boolean volumeOn = SETTING_MANAGER.isVolumeOn();
        int volumeLevel = SETTING_MANAGER.getVolumeLevel();
        volumeSlider.setValue(volumeLevel);
        setupVolume(volumeLevel, volumeOn);
        volumeSlider.valueProperty().addListener((observable, oldValue, newValue) -> setupVolume(newValue, true));
        volumeSlider.setOnMouseReleased(e -> saveVolume(true));
    }

    /**
     * Switches volume on/off.
     */
    @FXML
    private void switchVolume() {
        boolean newValue = !SETTING_MANAGER.isVolumeOn();
        volumeImage.setImage(newValue ? onImage : muteImage);
        SETTING_MANAGER.saveVolume(newValue);
    }

    /**
     * Sets up label and image on panel.
     * @param value volume level value
     * @param on if true, volume is on, false otherwise
     */
    private void setupVolume(Number value, boolean on) {
        volumeLabel.setText(value.intValue() + "%");
        volumeImage.setImage(on ? onImage : muteImage);
    }

    /**
     * Saves volume level and on/off setting.
     * @param on if true, volume is on, mute otherwise
     */
    private void saveVolume(boolean on) {
        SETTING_MANAGER.saveVolumeLevel((int) volumeSlider.getValue());
        SETTING_MANAGER.saveVolume(on);
    }
}
