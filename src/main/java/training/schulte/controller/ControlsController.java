package training.schulte.controller;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

/**
 * Controls.fxml controller.
 * @author Alexey Chalov
 */
public class ControlsController {

    @FXML
    private VBox contentVBox;

    /**
     * Closes dialog.
     */
    @FXML
    private void close() {
        contentVBox.getScene().getWindow().hide();
    }
}
