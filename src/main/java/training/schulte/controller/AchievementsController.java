package training.schulte.controller;

import static javafx.collections.FXCollections.observableArrayList;

import java.util.List;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import training.schulte.DataBridge;
import training.schulte.listcell.GameTypeListCell;
import training.schulte.listcell.IntervalListCell;
import training.schulte.manager.RankManager;
import training.schulte.model.GameResult;
import training.schulte.model.GameType;
import training.schulte.model.Interval;

/**
 * Achievements.fxml controller.
 * @author Alexey Chalov
 */
public class AchievementsController {

    private static final DataBridge DATA_BRIDGE = DataBridge.instance();
    private static final RankManager RANK_MANAGER = RankManager.instance();

    @FXML
    private ComboBox<Interval> intervalCombobox;
    @FXML
    private ComboBox<GameType> gameTypeCombobox;
    @FXML
    private TableView<GameResult> resultsTable;
    @FXML
    private TableColumn<GameResult, String> fieldSize;
    @FXML
    private TableColumn<GameResult, Integer> completedGames;
    @FXML
    private TableColumn<GameResult, String> bestTime;
    @FXML
    private TableColumn<GameResult, String> averageTime;

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        List<GameType> completedGameTypes = RANK_MANAGER.getCompletedGameTypes();
        gameTypeCombobox.setValue(RANK_MANAGER.getGameType());
        gameTypeCombobox.setItems(observableArrayList(completedGameTypes));
        gameTypeCombobox.setButtonCell(new GameTypeListCell());
        gameTypeCombobox.setCellFactory((p) -> new GameTypeListCell());

        initIntervalCombobox();

        fieldSize.setCellValueFactory(new PropertyValueFactory<>("displayGameSize"));
        completedGames.setCellValueFactory(new PropertyValueFactory<>("completedGames"));
        bestTime.setCellValueFactory(new PropertyValueFactory<>("bestTime"));
        averageTime.setCellValueFactory(new PropertyValueFactory<>("averageTime"));

        resultsTable.setMouseTransparent(true);

        initGameResults();
    }

    /**
     * Main menu navigation.
     */
    @FXML
    private void mainMenu() {
        DATA_BRIDGE.getApplication().buildStage();
    }

    /**
     * Change game type event handler.
     */
    @FXML
    private void changeGameType() {
        RANK_MANAGER.setGameType(gameTypeCombobox.getValue());
        initIntervalCombobox();
        initGameResults();
    }

    /**
     * Change time interval event handler.
     */
    @FXML
    private void changeInterval() {
        RANK_MANAGER.setInterval(intervalCombobox.getValue());
        initGameResults();
    }

    /**
     * Initializes time interval combobox.
     */
    private void initIntervalCombobox() {
        intervalCombobox.setValue(RANK_MANAGER.getInterval());
        intervalCombobox.setItems(observableArrayList(RANK_MANAGER.getAllowedIntervals()));
        intervalCombobox.setButtonCell(new IntervalListCell());
        intervalCombobox.setCellFactory((p) -> new IntervalListCell());
    }

    /**
     * Initializes game results table.
     */
    private void initGameResults() {
        resultsTable.setItems(observableArrayList(
            RANK_MANAGER.getGameResults()
        ));
        int cellSize = 25;
        resultsTable.setFixedCellSize(cellSize);
        resultsTable.prefHeightProperty().bind(Bindings.size(resultsTable.getItems()).multiply(cellSize).add(30));
    }
}
