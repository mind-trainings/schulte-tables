package training.schulte.controller;

import static javafx.collections.FXCollections.observableArrayList;
import static training.schulte.util.GameUtil.getClassPathResourceUrl;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.SneakyThrows;
import training.schulte.DataBridge;
import training.schulte.listcell.LocaleListCell;
import training.schulte.manager.RankManager;
import training.schulte.manager.SettingManager;
import training.schulte.util.GameUtil;

/**
 * Main.fxml controller.
 * @author Alexey Chalov
 */
public class MainController {

    @FXML
    private ComboBox<Locale> languageCombobox;
    @FXML
    private Button achievementsButton;

    private static final DataBridge DATA_BRIDGE = DataBridge.instance();
    private static final SettingManager SETTING_MANAGER = SettingManager.instance();
    private static final RankManager RANK_MANAGER = RankManager.instance();

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        languageCombobox.setItems(observableArrayList(GameUtil.LOCALE_RU, GameUtil.LOCALE_UK));
        languageCombobox.setValue(SETTING_MANAGER.getLocale());
        languageCombobox.setButtonCell(new LocaleListCell());
        languageCombobox.setCellFactory((p) -> new LocaleListCell());
        achievementsButton.setDisable(RANK_MANAGER.isResultsEmpty());
    }

    /**
     * New game navigation.
     */
    @FXML
    @SneakyThrows
    private void newGame() {
        DATA_BRIDGE.getApplication().getStage().getScene().setRoot(
            new FXMLLoader(getClassPathResourceUrl("view/NewGame.fxml"), DATA_BRIDGE.getResourceBundle()).load()
        );
    }

    /**
     * Achievements navigation.
     */
    @FXML
    @SneakyThrows
    private void achievements() {
        DATA_BRIDGE.getApplication().getStage().getScene().setRoot(
            new FXMLLoader(getClassPathResourceUrl("view/Achievements.fxml"), DATA_BRIDGE.getResourceBundle()).load()
        );
    }

    /**
     * Opens controls dialog.
     */
    @FXML
    @SneakyThrows
    private void controls() {
        ResourceBundle resourceBundle = DATA_BRIDGE.getResourceBundle();
        Stage windowStage = DATA_BRIDGE.getApplication().getStage();
        Scene windowScene = DATA_BRIDGE.getScene();

        Stage dialog = new Stage();
        dialog = new Stage(StageStyle.TRANSPARENT);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(windowScene.getWindow());
        VBox content = new FXMLLoader(getClassPathResourceUrl("view/Controls.fxml"), resourceBundle).load();
        dialog.setX(windowStage.getX() + (windowScene.getWidth() - content.getPrefWidth()) / 2 + 5);
        dialog.setY(windowStage.getY() + (windowScene.getHeight() - content.getPrefHeight()) / 2 + 30);
        Scene scene = new Scene(content);
        scene.getStylesheets().addAll(windowScene.getStylesheets());
        scene.setFill(Color.TRANSPARENT);
        dialog.setScene(scene);
        dialog.showAndWait();
    }

    /**
     * Change locale event handler.
     */
    @FXML
    private void changeLocale() {
        Locale locale = languageCombobox.getValue();
        if (!SETTING_MANAGER.getLocale().equals(locale)) {
            SETTING_MANAGER.saveLocale(locale);
            DATA_BRIDGE.getApplication().buildStage();
        }
    }

    /**
     * Exit event handler.
     */
    @FXML
    private void exit() {
        DataBridge.instance().getApplication().stop();
        System.exit(0);
    }
}
