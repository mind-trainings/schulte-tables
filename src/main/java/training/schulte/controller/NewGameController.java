package training.schulte.controller;

import static javafx.collections.FXCollections.observableArrayList;
import static training.schulte.util.GameUtil.getClassPathResourceUrl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Popup;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import training.schulte.DataBridge;
import training.schulte.listcell.GameSizeListCell;
import training.schulte.listcell.GameTypeListCell;
import training.schulte.manager.SettingManager;
import training.schulte.model.GameSize;
import training.schulte.model.GameType;

/**
 * NewGame.fxml controller.
 * @author Alexey Chalov
 */
public class NewGameController {

    private static final DataBridge DATA_BRIDGE = DataBridge.instance();
    private static final SettingManager SETTING_MANAGER = SettingManager.instance();

    @FXML
    private ImageView previewImageView;
    @FXML
    private ComboBox<GameType> gameTypeCombobox;
    @FXML
    private ComboBox<GameSize> gameSizeCombobox;

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        gameTypeCombobox.setValue(SETTING_MANAGER.getGameType());
        gameTypeCombobox.setItems(observableArrayList(GameType.values()));
        gameTypeCombobox.setButtonCell(new GameTypeListCell());
        gameTypeCombobox.setCellFactory(p -> new GameTypeListCell());

        gameSizeCombobox.setValue(SETTING_MANAGER.getGameSize());
        List<GameSize> values =
            IntStream.range(SETTING_MANAGER.getNumericGameMinSize(), SETTING_MANAGER.getNumericGameMaxSize() + 1)
                     .mapToObj((i) -> new GameSize(i, i)).collect(Collectors.toList());
        gameSizeCombobox.setItems(observableArrayList(values));
        gameSizeCombobox.setButtonCell(new GameSizeListCell());
        gameSizeCombobox.setCellFactory(p -> new GameSizeListCell());
        setupPreviewImage();
    }

    /**
     * Play game navigation.
     */
    @FXML
    @SneakyThrows
    private void startGame() {
        DATA_BRIDGE.getApplication().getStage().getScene().setRoot(
            new FXMLLoader(getClassPathResourceUrl("view/Game.fxml"), DATA_BRIDGE.getResourceBundle()).load()
        );
    }

    /**
     *  Main menu navigation.
     */
    @FXML
    private void mainMenu() {
        DATA_BRIDGE.getApplication().buildStage();
    }

    /**
     * Change game type event handler.
     */
    @FXML
    private void changeGameType() {
        SETTING_MANAGER.saveGameType(gameTypeCombobox.getValue());
        setupPreviewImage();
    }

    /**
     * Change game size event handler.
     */
    @FXML
    private void changeGameSize() {
        SETTING_MANAGER.saveGameSize(gameSizeCombobox.getValue());
    }

    /**
     * Show game extra settings.
     */
    @FXML
    @SneakyThrows
    private void showGameSettings() {
        Stage stage = DATA_BRIDGE.getApplication().getStage();
        Popup gameSettingsPopup = new Popup();
        gameSettingsPopup.setAutoHide(true);

        Pane settingsPane = new FXMLLoader(
            getClassPathResourceUrl("view/setting/" + gameTypeCombobox.getValue().name() + ".fxml"),
            DATA_BRIDGE.getResourceBundle()
        ).load();

        Parent root = stage.getScene().getRoot();
        Bounds boundsInScreen = root.localToScreen(root.getBoundsInLocal());
        gameSettingsPopup.setX(boundsInScreen.getMinX() + boundsInScreen.getWidth() / 2 - settingsPane.getMinWidth() / 2);
        gameSettingsPopup.setY(boundsInScreen.getMinY() + boundsInScreen.getHeight() / 2 - settingsPane.getMinHeight() / 2);

        gameSettingsPopup.getContent().add(settingsPane);
        gameSettingsPopup.show(stage);
    }

    /**
     * Sets up preview image.
     */
    private void setupPreviewImage() {
        previewImageView.setImage(
            new Image(getClassPathResourceUrl("image/preview/" + gameTypeCombobox.getValue().name().toLowerCase() + ".png").toExternalForm())
        );
    }
}
