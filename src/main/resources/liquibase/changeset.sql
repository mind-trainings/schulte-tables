--liquibase formatted sql

--changeset raven:1
create table setting(
    mnemo varchar(32) not null,
    value varchar(32),
    primary key(mnemo)
);

/* user settings */
insert into setting(mnemo, value) values('LOCALE', null);
insert into setting(mnemo, value) values('GAME_TYPE', 'CLASSICAL_NUMERIC');
insert into setting(mnemo, value) values('GAME_SIZE_WIDTH', '5');
insert into setting(mnemo, value) values('GAME_SIZE_HEIGHT', '5');
insert into setting(mnemo, value) values('VOLUME', '1');
/* system settings */
insert into setting(mnemo, value) values('MAX_RANK', '5');
insert into setting(mnemo, value) values('NUMERIC_GAME_MIN_SIZE', '3');
insert into setting(mnemo, value) values('NUMERIC_GAME_MAX_SIZE', '12');

create table game_result(
    game_type        varchar(32) not null,
    game_size_width  int         not null,
    game_size_height int         not null,
    complete_date    timestamp   not null default current_timestamp,
    play_time        int         not null
);

/* main results view */
create index game_result_idx01 on game_result(game_type, complete_date desc);
/* min play time search */
create index game_result_idx02 on game_result(game_type, game_size_width, game_size_height, play_time desc);

--changeset raven:2
update setting set mnemo = 'VOLUME_ON' where mnemo = 'VOLUME';
insert into setting(mnemo, value) values('VOLUME_LEVEL', '30');

--changeset raven:3
create table game_setting(
    game_type     varchar(32) not null,
    setting_name  varchar(32) not null,
    setting_value varchar(32) not null,
    primary key(game_type, setting_name)
);

insert into game_setting(game_type, setting_name, setting_value) values('CLASSICAL_NUMERIC', 'COUNTDOWN', '0');
insert into game_setting(game_type, setting_name, setting_value) values('BLACK_RED_NUMERIC', 'COLORIZE_BACKGROUND', '1');
