**Schulte tables game**

Currently supports 2 modes - classical numeric and black-red Gorbov table.

**Игра-тренажер таблиц Шульте**

Поддерживает на текущий момент 2 режима - классическую числовую и красно-черную Горбова.

***
Technologies / Стек технологий: Java SE 11+, JavaFX, Lombok, H2 database, JGroups, Commons-CLI, Maven

***
How to run / Запуск игры:

    Maven: mvn javafx:run

    Java SE 11+: main class training.schulte.SchulteTable,
                 VM arguments --module-path <JAVAFX_SDK_PATH>/lib  --add-modules javafx.controls,javafx.fxml,javafx.media

***
TODO list / Список доработок:

    //TODO: symbol table
    //TODO: use bordered pane in game area?
    //TODO: single jar? install4j? 0install.net?
	//TODO: опция после того как ты отмечаешь цифру, оставшиеся перемешиваются